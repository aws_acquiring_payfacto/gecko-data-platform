### DMS/Aurora setup through cloudformation

#### Included files

```bash
.
├── README.MD                          <-- Thisinstructions file
├── templates                          <-- Cloudformation templates
│   └── master-stack.yaml              <-- Orchestarte resource creation
│   └── dms-essential.yaml             <-- DMS cloudformation template
│   └── dms-prerequisites.yaml         <-- prerequisites required to alow logging
│   └── db-credentials-secret.yaml     <-- run on to be deployed accounts to setup secret manager which holds db credentials
│   └── dms-monitoring.yaml            <-- contains topics for alerts
│   └── operations                     <-- 
│   │   └── alet-stack.yaml            <-- contains topics for alerts
│   └── security                       <-- 
│   │   └── kms-key.yaml               <-- KMS customer managed CMK for RDS service
│   └── aurora.yaml                    <-- provision an RDS cluster using Postgresql engine
│   └── s3.yaml                        <-- staging area to store raw data
│   └── pipeline                       <-- 
│       └── shared_services            <-- contains dependent cloudformation templates for CICD automation
│       │   └── kms.yaml               <-- run on shared services accounts to setup S3 bucket and KMS encryption for it
│       │   └── codepipeline.yaml      <-- run on shared services accounts to setup code pipeline for dms repo
│       │   └── repository.yaml        <-- setting up repository in shared services
│       └── group_accounts             <-- 
│           └── setup-iam-roles.yaml   <-- run on to be deployed accounts to setup required iam roles for cross account deployments
├── config                             <-- 
│   └── buildspec.yml                  <-- code build template for dms
│   └── qa.json                        <-- configuration for dev code build stage
│   └── staging.json                   <-- configuration for pre-prod build stage
│   └── prod.json                      <-- configuration for prod code build stage
```

### Prerequisites
1. The VPC CloudFormation(baseline-payfacto-landing-zone-vpc) stack requires two Availability Zones for setting up the public and private subnets. Make sure to select an AWS Region that has at least two Availability Zones.
2. Create an EC2 key pair using Amazon EC2 console in the AWS Region where you are planning to set up the CloudFormation stacks. Make sure to save the private key, as this is the only time you can do this. You use this EC2 key pair as an input parameter while setting up the Amazon Linux bastion host CloudFormation stack. Key Pair Name format must be as follow 
[environment]-apgbastion-[region] 
e.g.:   dev-apgbastion-us-east-1, 
        qa-apgbastion-us-east-1, 
        staging-apgbastion-us-east-1
        prod-apgbastion-us-east-1
3. Create an Amazon S3 bucket and upload the sample database bootstrap AWS Lambda zip file, as follows.

    Create an S3 bucket in the same AWS Region, where you planning to set up the Aurora PostgreSQL DB cluster. No additional settings are required within the bucket configuration. Bucket naming convention: _payfacto-apgbootstrap-scripts-[AWSAccount]-[Region]_
    e.g.:   payfacto-apgbootstrap-scripts-918259764903-us-east-1
            payfacto-apgbootstrap-scripts-039010030369-us-east-1 
            payfacto-apgbootstrap-scripts-761944281397-us-east-1 
            payfacto-apgbootstrap-scripts-471454517700-us-east-1 

    In the bucket, create a folder (prefix) called "lambda".
    Upload the zip files(dbbootstrap.zip, dbonboarduser.zip) to the S3 bucket folder. No additional settings are required on the upload screen.
    You use the S3 bucket name along with the S3 key prefix as input parameters while setting up the Aurora DB cluster CloudFormation Stack. This Zip file is used to create a Lambda function written in Python 3.6, which uses PyGreSQL module to connect to the Aurora PostgreSQL cluster and bootstrap it upon creation.
    Follow instructions described in assets/functions/dbbootstrap/install.sh


### Deployment Instructions

| Action | Team | Details |
|--------|:----:|--------:|
| 1. Deploy pipeline/shared_services/{region}/kms.yaml | Configuration Management | Sets up KMS key and S3 bucket in each region. |
| 2. Deploy pipeline/group_accounts/setup_iam_roles.yaml in every account | Application Team | Sets up the IAM roles for cross account access. Needs the KMS key arn and S3 bucket name from step 1 |
| 3. Deploy shared_services/{region}/codepipeline.yaml | Configuration Management | Sets up S3 bucket policies and the build/release pipeline. |


## Create CodePipeline in shared service

```
aws cloudformation create-stack --stack-name infra-rds-deployment-topics --template-body file://templates/pipeline/shared_services/repository.yaml --parameters file://templates/pipeline/shared_services/repository-params.json --profile qa
```

push code to new created repository on CodeCommit (codecommit::us-east-1://gecko-dataplatform)
git remote add origin codecommit::us-east-1://gecko-dataplatform
```
aws cloudformation create-stack --stack-name gecko-dataplatform-KmsSetupForCodepipeline --template-body file://templates/pipeline/shared_services/kms.yaml --parameters file://templates/pipeline/shared_services/kms-params.json --capabilities CAPABILITY_NAMED_IAM --profile qa
```

Run following cloudformation template in each envirnment account (dev/qa/staging/prod), suply ArtifactBucket name and CMKARN from KMS.yaml(above cloudformation stack)

```
aws cloudformation create-stack --stack-name gecko-dataplatform-CloudformationRoleSetup --template-body file://templates/pipeline/group_accounts/setup-iam-roles.yaml --parameters file://templates/pipeline/group_accounts/setup-iam-roles-params.json --capabilities CAPABILITY_NAMED_IAM --profile dev
```

Update parameters file by suplying corresponding roles from previous steps(Repository Topics, CFN Deployer role, Code Pipeline role and repository CMKARN)

```
aws cloudformation create-stack --stack-name gecko-dataplatform-pipeline --template-body file://templates/pipeline/shared_services/codepipeline.yaml --parameters file://templates/pipeline/shared_services/codepipeline-params.json --capabilities CAPABILITY_NAMED_IAM --profile qa
```

Steps below are intended to run manually if code pipeline is not established yet
aws s3 sync ./templates s3://payfacto-codebase/gecko-dp/templates --delete  --profile default
aws s3 sync ./etlscripts s3://payfacto-codebase/gecko-dp/etlscripts --delete   --profile default

```
aws cloudformation create-stack --stack-name gecko-dataplatform-master --template-body file://templates/master-stack.yaml --parameters file://templates/local_tests/master-stack-params.json --capabilities CAPABILITY_NAMED_IAM --on-failure DO_NOTHING --profile default

```
Useful CLI COmmands

Delete a secret without retention period:
```
aws secretsmanager delete-secret --force-delete-without-recovery --secret-id acquiring-merchant-management-core-postgres --profile qa
```

Add new connection Heroku MongoDB in secret manager
```
aws secretsmanager create-secret --name external/omr-mongodb-qa \
    --description "Heroku Mongo DB database secret created with the CLI" \
    --secret-string file://mongoqacreds.json \
    --profile dev
``` 

