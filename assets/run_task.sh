stack_name=gecko-data-platform
echo "Stack Name: ${stack_name}"
task_arn=$(aws cloudformation describe-stacks --stack-name "${stack_name}" --region "${AWS_REGION}" --profile "${AWS_PROFILE}" --output text | grep "${stack_name}"-DMSReplicationTaskArn | awk '{print $7}')
echo "Task ARN: ${task_arn}"
echo "Starting task..."
aws dms start-replication-task --start-replication-task-type start-replication --replication-task-arn ${task_arn}
aws dms wait replication-task-running --filters Name=replication-task-arn,Values=${task_arn}

if [ $? -ne 0 ]; then
    echo "Error starting replication task"
    exit 3
fi

echo "Replication task is started"