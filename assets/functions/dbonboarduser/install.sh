#1. Install new libraries.
pip install --target ./package pgdb

#2. Create a ZIP archive of the dependencies.
~/dbbootstrap$ cd package
~/dbbootstrap/package$ zip -r9 "${OLDPWD}"/dbonboarduser.zip .

#3. Add your function code to the archive.
~/dbbootstrap/package$ cd $OLDPWD
~/dbbootstrap$ zip -g dbonboarduser.zip dbonboarduser.py cfnresponse.py

#4. Upload zip file to S3 bucket
aws s3 cp dbonboarduser.zip s3://payfacto-apgbootstrap-scripts-918259764903-us-east-1/lambda/ --profile dev
aws s3 cp dbonboarduser.zip s3://payfacto-apgbootstrap-scripts-039010030369-us-east-1/lambda/ --profile qa
aws s3 cp dbonboarduser.zip s3://payfacto-apgbootstrap-scripts-761944281397-us-east-1/lambda/ --profile staging
aws s3 cp dbonboarduser.zip s3://payfacto-apgbootstrap-scripts-471454517700-us-east-1/lambda/ --profile prod

#5. Or update the function code if the function has been deployed, the function name you can find in output page of aurora.yaml CFN
aws lambda update-function-code --profile dev --zip-file fileb://dbonboarduser.zip --function-name arn:aws:lambda:us-east-1:918259764903:function:infra-rds-app-dev-AuroraStac-DBOnboardUserLambdaFn-DZ7MJ99S9HF1
