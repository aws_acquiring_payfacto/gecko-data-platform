#1. Install new libraries.
pip install --target ./package Pillow

#2. Create a ZIP archive of the dependencies.
~/dbbootstrap$ cd package
~/dbbootstrap/package$ zip -r9 ${OLDPWD}/dbbootstrap.zip .

#3. Add your function code to the archive.
~/dbbootstrap/package$ cd $OLDPWD
~/dbbootstrap$ zip -g dbbootstrap.zip dbbootstrap.py cfnresponse.py

#4. Upload zip file to S3 bucket
aws s3 cp dbbootstrap.zip s3://payfacto-apgbootstrap-scripts-918259764903-us-east-1/lambda/ --profile dev
aws s3 cp dbbootstrap.zip s3://payfacto-apgbootstrap-scripts-039010030369-us-east-1/lambda/ --profile qa
aws s3 cp dbbootstrap.zip s3://payfacto-apgbootstrap-scripts-761944281397-us-east-1/lambda/ --profile staging
aws s3 cp dbbootstrap.zip s3://payfacto-apgbootstrap-scripts-471454517700-us-east-1/lambda/ --profile prod
#5. Or update the function code if the function has been deployed, the function name you can find in output page of aurora.yaml CFN
aws lambda update-function-code --function-name infra-rds-app-dev-AuroraStack-DBBootStrapLambdaFn-QW2AP2C4SCSU --zip-file fileb://dbbootstrap.zip
