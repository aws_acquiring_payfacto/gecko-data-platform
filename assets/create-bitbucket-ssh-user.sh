#!/bin/bash
# Bitbucket Bootstrapping
# authors:
 
echo "Executing bitbucket user creation script"
set -e # Will exit on first error

echo "Creating bitbucket user"
groupadd bitbucket
useradd -m -g bitbucket bitbucket

echo "Creating ssh key"
HOME_FOLDER=/home/bitbucket
mkdir -p "$HOME_FOLDER/.ssh"
chmod 700 "$HOME_FOLDER/.ssh"
ssh-keygen -t rsa -b 4096 -q -N "" -f "$HOME_FOLDER/.ssh/id_rsa"

echo "Updating bitbucket-ssh-key secret with key"
aws secretsmanager put-secret-value --region us-east-1 --secret-id bitbucket-ssh-key --secret-string file://"$HOME_FOLDER/.ssh/id_rsa"
 
echo "Updating ~/.ssh/authorized_keys with public key"
ssh-keygen -y -f "$HOME_FOLDER/.ssh/id_rsa" >> "$HOME_FOLDER/.ssh/authorized_keys"
chmod 600 "$HOME_FOLDER/.ssh/authorized_keys"

chown -R bitbucket:bitbucket "$HOME_FOLDER/.ssh"

echo "Cleaning up"
rm "$HOME_FOLDER/.ssh/id_rsa"
rm "$HOME_FOLDER/.ssh/id_rsa.pub"

echo "Bitbucket Bootstrapping completed"
