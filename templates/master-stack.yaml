AWSTemplateFormatVersion: 2010-09-09
Description: Setup Database Migration Service and Aurora Postgres. DMS Version 3.3.1 and Aurora Postgres Version 10.7

#############################
# PARAMETERS                #
#############################
Parameters:
  ParentVPCStack:
    Description: "Provide Stack name of parent VPC stack based on payfacto-landing-zone-vpc yaml template. Refer Cloudformation dashboard in AWS Console to get this."
    Type: String
    MinLength: "1"
    MaxLength: "128"
    AllowedPattern: '^[a-zA-Z]+[0-9a-zA-Z\-]*$'
  SystemName:
    Type: String
    Default: infra
    Description: >-
      Identifier of the system to which the resource belongs.
      A system may have zero or more applications that make up the system. 
      Typically, always include so that isolation between systems, 
      workload instances and cloud foundation resources can be achieved
  ApplicationName:
    Type: String
    Default: rds-app
    Description: An optional identifier of the application to which the resource belongs.
  DocDbApplicationName:
    Type: String
    Default: docdb-app
    Description: An optional identifier of the application to which the resource belongs.

  SecretPurpose:
    Type: String
    Default: masteruser
    Description: The resource's purpose or function. Typically included in resource names.
  Environment:
    Type: String
    Description: Environment (dev, qa, staging, prod)
    Default: dev
    AllowedValues:
      - dev
      - qa
      - staging
      - prod
  BusinessUnit:
    Type: String
    Description: Business unit
    Default: payfacto
    AllowedValues:
      - payfacto
  RdsInstanceClass:
    Type: String
    Description: RDS Instance Class
    Default: "db.t2.medium"
    AllowedValues:
      - db.t2.small
      - db.t2.medium
      - db.t3.small
      - db.t3.medium
      - db.c4.large
      - db.r5.large
      - db.r5.xlarge
  DocDBInstanceClass:
    Description: "Instance class. Please refer to: https://docs.aws.amazon.com/documentdb/latest/developerguide/db-instance-classes.html#db-instance-classes-by-region"
    Type: "String"
    AllowedValues:
      - db.t3.medium
      - db.r5.large
      - db.r5.xlarge
      - db.r5.2xlarge
      - db.r5.4xlarge
      - db.r5.12xlarge
      - db.r5.24xlarge
    ConstraintDescription: "Instance type must be of the ones supported for the region. Please refer to: https://docs.aws.amazon.com/documentdb/latest/developerguide/db-instance-classes.html#db-instance-classes-by-region"

  DmsInstanceStorage:
    Type: Number
    Default: 500
  ReplicationInstanceClass:
    Type: String
    Description: Replication Instance Class
    Default: "dms.t2.small"
    AllowedValues:
      - dms.t2.micro
      - dms.t2.small
      - dms.t2.medium
      - dms.t3.medium
      - dms.t2.large
      - dms.c4.large
      - dms.c4.xlarge
      - dms.r4.xlarge
  ReplicationInstanceName:
    Type: String
    Description: Replication Instance Name
  # SourceEndpointName:
  #   Type: String
  #   Description: Source Endpoint Name
  FullLoadAndCdcBucketName:
    Type: String
    Description: Full load and cdc bucket name
  TargetEndpointNameFullAndCdc:
    Type: String
    Description: Target Endpoint Name
  # MasterUsername:
  #   Description: >-
  #     The user name that is associated with the master user account for the
  #     cluster that is being created
  #   Type: String
  #   Default: awsuser
  #   AllowedPattern: "([a-z])([a-z]|[0-9])*"
  # MasterUserPassword:
  #   Description: >-
  #     The password that is associated with the master user account for the
  #     cluster that is being created. Minimum eight characters, at least one
  #     uppercase letter, one lowercase letter, one number and one special
  #     character
  #   Type: String
  #   NoEcho: "true"
  #   Default: "Password#123"
  #   AllowedPattern: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
  Engine:
    Description: "Aurora engine and version"
    Type: String
    AllowedValues:
      - "5.6.mysql-aurora.1.19.1" # aws rds describe-db-engine-versions --engine aurora --query 'DBEngineVersions[?contains(SupportedEngineModes,`provisioned`)].EngineVersion'
      - aurora
      - "5.7.mysql-aurora.2.04.3" # aws rds describe-db-engine-versions --engine aurora-mysql --query 'DBEngineVersions[?contains(SupportedEngineModes,`provisioned`)].EngineVersion'
      - "5.7.mysql-aurora.2.03.4"
      - "aurora-mysql"
      - "aurora-postgresql-10.7" # aws rds describe-db-engine-versions --engine aurora-postgresql --query 'DBEngineVersions[?contains(SupportedEngineModes,`provisioned`)].EngineVersion'
      - "aurora-postgresql-10.6"
      - "aurora-postgresql-10.5"
      - "aurora-postgresql-10.4"
      - "aurora-postgresql-9.6.12"
      - "aurora-postgresql"
    Default: aurora-postgresql-10.7
  DBAllocatedStorage:
    Default: "50"
    Description: The size of the database (GiB)
    Type: Number
    MinValue: "5"
    MaxValue: "2024"
    ConstraintDescription: must be between 20 and 65536 GiB.
  # DBSubnet1:
  #   Type: AWS::EC2::Subnet::Id
  #   Description: ID of Subnet 1
  #   AllowedPattern: "subnet-[a-zA-Z0-9]+"
  #   ConstraintDescription: must be an valid subnet ID in the selected Virtual Private Cloud.
  # DBSubnet2:
  #   Type: AWS::EC2::Subnet::Id
  #   Description: ID of Subnet 2
  #   AllowedPattern: "subnet-[a-zA-Z0-9]+"
  #   ConstraintDescription: must be an valid subnet ID in the selected Virtual Private Cloud.
  # VPCID:
  #   Type: String
  #   Description: VPC ID
  # VPCName:
  #   Description: The name of the VPC being created.
  #   Type: String
  #   Default: "payfacto"
  # InternetGatewayId:
  #   Type: String
  # PublicRouteTableId:
  #   Type: String
  # NATGatewayId:
  #   Type: String
  # ElasticIP0AllocId:
  #   Type: String
  # Public0CIDR:
  #   Type: String
  # Private0CIDR:
  #   Type: String
  # Private1CIDR:
  #   Type: String
  ApplyPrereq:
    Type: String
    Description: Allowed values are true or false, to mention if pre requisites are required
    Default: false
    AllowedValues:
      - true
      - false
  S3AccessRoleName:
    Type: String
    Description: role name
  # DefaultSecurityGroup:
  #   Type: AWS::EC2::SecurityGroup::Id
  #   Description: security group id
  PubliclyAccessible:
    Default: "false"
    Description: Make cluster available outside the VPC
    Type: String
    AllowedValues: ["true", "false"]
  RDSDBName:
    Type: String
    Description: db instance name
  DocDBClusterName:
    Type: String
    Description: document db cluster name
  DBUsername:
    Description: Database master username
    Type: String
    MinLength: "1"
    MaxLength: "16"
    AllowedPattern: "^[a-zA-Z]+[0-9a-zA-Z_]*$"
    ConstraintDescription: Must start with a letter. Only numbers, letters, and _ accepted. max length 16 characters
  DocDBMasterUser:
    Description: Database master username
    Type: String
    MinLength: "1"
    MaxLength: "16"
    AllowedPattern: "^[a-zA-Z]+[0-9a-zA-Z_]*$"
    ConstraintDescription: Must start with a letter. Only numbers, letters, and _ accepted. max length 16 characters

  DBSubnetGroupName:
    Type: String
    Description: db subnet group name
  DocDBSubnetGroupName:
    Type: String
    Description: db subnet group name
  DBSnapshotArn:
    Type: String
    Description: db snapshot arn
    Default: ""
  DocDBSnapshotArn:
    Description: "Document DB Snapshot ID"
    Type: String
    Default: ""
  TemplatePath:
    Type: String
    Description: child template path
    Default: "child-templates"
  ScriptPath:
    Type: String
    Description: child template path
  # Email:
  #   Description: "Specify email address to will receive alerts"
  #   Type: String
  #   Default: ""
  HttpsEndpoint:
    Description: "Optional HTTPS endpoint that will receive alerts via POST requests"
    Type: String
    Default: ""
  FallbackEmail:
    Description: "Optional email address that will receive alerts if alerts are not delivered"
    Type: String
    Default: ""
  # BastionHost parameters
  NotificationList:
    Type: String
    Description: The Email notification list is used to configure a SNS topic for sending cloudwatch alarm notifications
    AllowedPattern: '^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'
    ConstraintDescription: provide a valid email address.
  KeyPairName:
    Description: >-
      Enter a Public/private key pair. If you do not have one in this AWS Region,
      create it before continuing
    Type: "AWS::EC2::KeyPair::KeyName"
  RemoteAccessCIDR:
    AllowedPattern: >-
      ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    ConstraintDescription: CIDR block parameter must be in the form x.x.x.x/x
    Description: Allowed CIDR block in the x.x.x.x/x format for external SSH access to the bastion host, specify Payfacto VPN gateway IP
    Type: String
  LambdaBootStrapS3Bucket:
    Description: Optional. Specify S3 bucket name for e.g. apgbootstrapscripts where Lambda DB Bootstrap Python script is stored.
    Default: ""
    Type: String
  LambdaBootStrapS3Key:
    Description: Optional. Specify S3 key for e.g. lambda/dbbootstrap.zip where Lambda DB Bootstrap Python script is stored.
    Default: ""
    Type: String
  ServiceOwnersEmailContact:
    Type: String
    Description: The ServiceOwnersEmailContact tag is used to designate business owner(s) email address associated with the given AWS resource for sending outage or maintenance notifications
    AllowedPattern: '^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'
    ConstraintDescription: provide a valid email address.
  EnableTCPForwarding:
    Type: String
    Description: Enable/Disable TCP Forwarding
    Default: "true"
    AllowedValues:
      - "true"
      - "false"
Conditions:
  CreatePreRequisites: !Equals [!Ref ApplyPrereq, true]
Resources:
  # VpcStack:
  #   Type: "AWS::CloudFormation::Stack"
  #   Properties:
  #     TemplateURL:
  #       Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/vpc.yaml"
  #     Parameters:
  #       VPCID: !Ref VPCID
  #       VPCName: !Ref VPCName
  #       InternetGatewayId: !Ref InternetGatewayId
  #       ElasticIP0AllocId: !Ref ElasticIP0AllocId
  #       Public0CIDR: !Ref Public0CIDR
  #       Private0CIDR: !Ref Private0CIDR
  #       Private1CIDR: !Ref Private1CIDR
  BastionStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/bastion.yaml"
      Parameters:
        ParentVPCStack: !Ref ParentVPCStack
        NotificationList: !Ref NotificationList
        KeyPairName: !Ref KeyPairName
        RemoteAccessCIDR: !Ref RemoteAccessCIDR
        Environment: !Ref Environment
        EnableTCPForwarding: !Ref EnableTCPForwarding
        ScriptLocation: !Ref ScriptPath
  AlertStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/operations/alert-stack.yaml"
      Parameters:
        Email: !Ref NotificationList
        HttpsEndpoint: !Ref HttpsEndpoint
        FallbackEmail: !Ref FallbackEmail
  RDSKeyStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/security/kms-key.yaml"
  AuroraStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/aurora.yaml"
      Parameters:
        DBName: !Ref RDSDBName
        DBUsername: !Ref DBUsername
        DBAllocatedStorage: !Ref DBAllocatedStorage
        DBSubnetGroupName: !Ref DBSubnetGroupName
        DBSnapshotArn: !Ref DBSnapshotArn
        Environment: !Ref Environment
        # DBSubnet1: !GetAtt VpcStack.Outputs.PublicSubnet0
        # DBSubnet2: !GetAtt VpcStack.Outputs.PrivateSubnet0
        # DefaultSecurityGroup: !GetAtt VpcStack.Outputs.SecurityGroupRDSPrivateExport
        # VPCID: !Ref VPCID
        ParentVPCStack: !Ref ParentVPCStack
        BastionSecurityGroupID: !GetAtt BastionStack.Outputs.BastionSecurityGroupID
        BastionHostRoleArn: !GetAtt BastionStack.Outputs.BastionHostRoleArn
        BastionHostRoleName: !GetAtt BastionStack.Outputs.BastionHostRoleName
        BastionEIP: !GetAtt BastionStack.Outputs.EIP
        BastionKeyPairName: !GetAtt BastionStack.Outputs.BastionKeyPairName
        SystemName: !Ref SystemName
        ApplicationName: !Ref ApplicationName
        SecretPurpose: !Ref SecretPurpose
        # MasterUsername: !Ref MasterUsername
        # MasterUserPassword: !Ref MasterUserPassword
        Engine: !Ref Engine
        DBInstanceClass: !Ref RdsInstanceClass
        AlertTopicArn: !GetAtt AlertStack.Outputs.AlertTopicArn
        RDSKeyId: !GetAtt RDSKeyStack.Outputs.KeyId
        PubliclyAccessible: !Ref PubliclyAccessible
        LambdaBootStrapS3Bucket: !Ref LambdaBootStrapS3Bucket
        LambdaBootStrapS3Key: !Ref LambdaBootStrapS3Key
        TemplatePath: !Ref TemplatePath
        ServiceOwnersEmailContact: !Ref ServiceOwnersEmailContact
      Tags:
        - Key: Name
          Value: DmsAuroraStack
        - Key: Classification
          Value: internal
        - Key: Compliance
          Value: internal
        - Key: Owner
          Value: !Ref BusinessUnit
        - Key: CostCenter
          Value: !Ref BusinessUnit
        - Key: Environment
          Value: !Ref Environment
        - Key: Application
          Value: "gecko-data-platform"
        - Key: Tier
          Value: other
  DocDBStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/documentdb.yaml"
      Parameters:
        DBClusterName: !Ref DocDBClusterName
        MasterUser: !Ref DocDBMasterUser
        DBSubnetGroupName: !Ref DocDBSubnetGroupName
        Environment: !Ref Environment
        ParentVPCStack: !Ref ParentVPCStack
        BastionSecurityGroupID: !GetAtt BastionStack.Outputs.BastionSecurityGroupID
        SystemName: !Ref SystemName
        ApplicationName: !Ref DocDbApplicationName
        SecretPurpose: !Ref SecretPurpose
        DBInstanceClass: !Ref DocDBInstanceClass
        RDSKeyId: !GetAtt RDSKeyStack.Outputs.KeyId
        ServiceOwnersEmailContact: !Ref ServiceOwnersEmailContact
        DocDBSnapshotArn: !Ref DocDBSnapshotArn
      Tags:
        - Key: Name
          Value: DocDBStack
        - Key: Classification
          Value: internal
        - Key: Compliance
          Value: internal
        - Key: Owner
          Value: !Ref BusinessUnit
        - Key: CostCenter
          Value: !Ref BusinessUnit
        - Key: Environment
          Value: !Ref Environment
        - Key: Application
          Value: "gecko-data-platform"
        - Key: Tier
          Value: other
  DMSPrerequisitesStack:
    Type: "AWS::CloudFormation::Stack"
    Condition: CreatePreRequisites
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/dms-prerequisites.yaml"
      Parameters:
        ApplyPrereq: !Ref ApplyPrereq
      Tags:
        - Key: Name
          Value: DmsPrerequisitesStack
        - Key: Classification
          Value: internal
        - Key: Compliance
          Value: pci
        - Key: Owner
          Value: !Ref BusinessUnit
        - Key: CostCenter
          Value: !Ref BusinessUnit
        - Key: Environment
          Value: !Ref Environment
        - Key: Application
          Value: "gecko-data-platform"
        - Key: Tier
          Value: other
  DMSS3Stack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/s3.yaml"
      Parameters:
        FullLoadAndCdcBucketName: !Ref FullLoadAndCdcBucketName
        Environment: !Ref Environment
        S3AccessRoleName: !Ref S3AccessRoleName
      Tags:
        - Key: Name
          Value: DmsS3Stack
        - Key: Classification
          Value: internal
        - Key: Compliance
          Value: none
        - Key: Owner
          Value: !Ref BusinessUnit
        - Key: CostCenter
          Value: !Ref BusinessUnit
        - Key: Environment
          Value: !Ref Environment
        - Key: Application
          Value: "gecko-data-platform"
        - Key: Tier
          Value: other
  MonitoringStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/dms-monitoring.yaml"
      Parameters:
        Environment: !Ref Environment
      Tags:
        - Key: Name
          Value: DmsMonitoringStack
        - Key: Classification
          Value: monitoring
        - Key: Compliance
          Value: none
        - Key: Owner
          Value: !Ref BusinessUnit
        - Key: CostCenter
          Value: !Ref BusinessUnit
        - Key: Environment
          Value: !Ref Environment
        - Key: Application
          Value: "gecko-data-platform"
        - Key: Tier
          Value: other
  DMSEssentialsStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL:
        Fn::Sub: "https://s3.amazonaws.com/${TemplatePath}/templates/dms-essentials.yaml"
      Parameters:
        Environment: !Ref Environment
        S3AccessRoleName: !Ref S3AccessRoleName
        TargetS3BucketFullAndCdc: !GetAtt DMSS3Stack.Outputs.TargetBucketName
        # DBSubnet1: !GetAtt VpcStack.Outputs.PrivateSubnet0
        # DBSubnet2: !GetAtt VpcStack.Outputs.PrivateSubnet1
        ParentVPCStack: !Ref ParentVPCStack
        DmsInstanceStorage: !Ref DmsInstanceStorage
        ReplicationInstanceClass: !Ref ReplicationInstanceClass
        ReplicationInstanceName: !Ref ReplicationInstanceName
        DMSSourceEndpointAurora: !GetAtt AuroraStack.Outputs.DMSAuroraSourceEndpointArn
        TargetEndpointNameFullAndCdc: !Ref TargetEndpointNameFullAndCdc
        DMSSourceEndpointDocumentDB: !GetAtt DocDBStack.Outputs.DMSDocDBSourceEndpointArn
        DMSSourceEndpointDocumentDB40: !GetAtt DocDBStack.Outputs.DMSDocDB40SourceEndpointArn
        ClusterSecurityGroupId: !GetAtt DocDBStack.Outputs.ClusterSecurityGroupId
        ClusterPort: !GetAtt DocDBStack.Outputs.ClusterPort
      Tags:
        - Key: Name
          Value: DmsEssentialsStack
        - Key: Classification
          Value: internal
        - Key: Compliance
          Value: pci
        - Key: Owner
          Value: !Ref BusinessUnit
        - Key: CostCenter
          Value: !Ref BusinessUnit
        - Key: Environment
          Value: !Ref Environment
        - Key: Application
          Value: "data lake"
        - Key: Tier
          Value: other
Outputs:
  AuroraClusterWriteEndpoint:
    Description: Endpoint for the newly created Aurora cluster
    Value: !GetAtt AuroraStack.Outputs.DNSName
  AuroraClusterReadEndpoint:
    Description: Endpoint for the newly created Aurora cluster
    Value: !GetAtt AuroraStack.Outputs.ReadDNSName
  # AuroraConnectionURL:
  #   Description: Cluster endpoint
  #   Value: !Join
  #     - ""
  #     - - jdbc:aurora://
  #       - !GetAtt "AuroraStack.Endpoint.Address"
  #       - ":"
  #       - !GetAtt "AuroraStack.Endpoint.Port"
  #       - /
  #       - !Ref "DatabaseName"
