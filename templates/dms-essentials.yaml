AWSTemplateFormatVersion: 2010-09-09   
Description: Setup Database Migration Service. Version 1.4  
  
#############################  
# PARAMETERS                #  
#############################  
Parameters:
  ParentVPCStack:
    Description: "Provide Stack name of parent VPC stack based on payfacto-landing-zone-vpc yaml template. Refer Cloudformation dashboard in AWS Console to get this."
    Type: String
    MinLength: "1"
    MaxLength: "128"
    AllowedPattern: '^[a-zA-Z]+[0-9a-zA-Z\-]*$'
    Default: baseline-payfacto-landing-zone-vpc
  Environment: 
    Type: String 
    Description: Environment (dev, qa, staging, prod) 
    Default: dev 
    AllowedValues: 
      - dev 
      - qa 
      - staging 
      - prod 
  DmsInstanceStorage: 
    Type: Number 
    Default: 6144 
  ReplicationInstanceClass: 
    Type: String 
    Description: Replication Instance Class  
    Default: "dms.t2.micro" 
    AllowedValues: 
      - dms.t2.micro 
      - dms.t2.small 
      - dms.t2.medium 
      - dms.t3.medium 
      - dms.t2.large 
      - dms.c4.large 
      - dms.c4.xlarge 
      - dms.r4.xlarge 
      - dms.r4.2xlarge 
  ReplicationInstanceName: 
    Type: String 
    Description: Replication Instance Name 
    Default: "gheco-dms-replication-instance"
  TargetEndpointNameFullAndCdc: 
    Type: String 
    Description: Target Endpoint Name 
    Default: s3-full-and-cdc-target
  S3AccessRoleName: 
    Type: String 
    Description: role name
    Default: "payfacto-dms-replication-s3-admin"
  TargetS3BucketFullAndCdc: 
    Type: String 
    Description: target s3 bucket name
    Default: payfacto-dms-target-v1-dev-918259764903-us-east-1
  DMSSourceEndpointAurora: 
    Type: String 
    Description: Aurora dms source endpoint arn 
    Default: "arn:aws:dms:us-east-1:918259764903:endpoint:COMX5N6KVW6SPTOMFQSPQGOUKDOM73QEQZF5TWY"
  DMSSourceEndpointDocumentDB:
    Type: String 
    Description: DocumentDB dms source endpoint arn 
    Default: ""
  DMSSourceEndpointDocumentDB40:
    Type: String 
    Description: DocumentDB dms source endpoint arn 
    Default: ""
  ClusterSecurityGroupId:
    Type: String
    Description: DocumentDB Security Group ID
  ClusterPort:
    Type: String
    Description: DocumentDN cluster port number
Resources:  
    S3AccessRole:  
      Type: "AWS::IAM::Role"  
      Properties:  
        AssumeRolePolicyDocument:  
          Version: '2012-10-17'  
          Statement:  
          - Effect: Allow  
            Principal:  
              Service: dms.amazonaws.com  
            Action: sts:AssumeRole  
        RoleName: !Ref S3AccessRoleName
        Policies:  
          - PolicyName: payfacto-dms-mgr-s3-admin 
            PolicyDocument:  
              Version: '2012-10-17'  
              Statement:  
                - Effect: Allow  
                  Resource:  
                  - !Sub 'arn:aws:s3:::${TargetS3BucketFullAndCdc}'
                  Action:  
                  - s3:ListBucket  
                - Effect: Allow  
                  Resource:  
                  - !Join ["",['arn:aws:s3:::', !Ref TargetS3BucketFullAndCdc,"/*"]] 
                  Action:  
                  - s3:PutObject  
                  - s3:DeleteObject 
    DMSTargetEndpointS3FullAndCdc:  
      Type: AWS::DMS::Endpoint  
      Properties:  
        EndpointIdentifier: !Sub ${TargetEndpointNameFullAndCdc}-${Environment}
        EngineName: s3  
        EndpointType: target  
        ExtraConnectionAttributes: "compressionType=GZIP;csvDelimiter=,;csvRowDelimiter=\n;dataFormat=parquet;datePartitionEnabled=false;enableStatistics=true;includeOpForFullLoad=true;parquetTimestampInMillisecond=true;parquetVersion=PARQUET_2_0;timestampColumnName=OP_TIME;"  
        S3Settings:  
          BucketName: !Ref TargetS3BucketFullAndCdc  
          ServiceAccessRoleArn: !GetAtt S3AccessRole.Arn
    # DMSSourceEndpointHerokuMongo:
    #   Type: AWS::DMS::Endpoint
    #   Properties:
    #     EndpointIdentifier: !Sub heroku-mongodb-source-${Environment}
    #     EngineName: mongodb
    #     EndpointType: source
    #     ExtraConnectionAttributes: "parallelLoadThreads=5;"
    #     MongoDbSettings:
    #       AuthMechanism: default
    #       AuthSource: admin
    #       AuthType: password
    #       DatabaseName: !Sub "{{resolve:secretsmanager:${MongoDbSecretName}:SecretString:dbname}}"
    #       DocsToInvestigate: String
    #       # ExtractDocId: "false"
    #       NestingLevel: "none"
    #       Password: !Sub "{{resolve:secretsmanager:${MongoDbSecretName}:SecretString:password}}"
    #       Port: !Sub "{{resolve:secretsmanager:${MongoDbSecretName}:SecretString:port}}"
    #       ServerName: !Sub "{{resolve:secretsmanager:${MongoDbSecretName}:SecretString:servername}}"
    #       Username: !Sub "{{resolve:secretsmanager:${MongoDbSecretName}:SecretString:username}}"
    DMSReplicationSubnetGroup:  
      Type: AWS::DMS::ReplicationSubnetGroup  
      Properties:  
        ReplicationSubnetGroupDescription: Subnets for DMS  
        SubnetIds:
          !Split [
            ",",
            { "Fn::ImportValue": !Sub "${ParentVPCStack}-SubnetsDataPrivate" },
          ]

    DMSSecurityGroup: 
      Type: "AWS::EC2::SecurityGroup"
      Properties:
        GroupDescription: !Ref "AWS::StackName"
        # SecurityGroupIngress:
        #   - IpProtocol: tcp
        #     FromPort: -1
        #     ToPort: !FindInMap [EngineMap, !Ref Engine, Port]
        #     Description: "Access to DMS Instance Security Group"
        VpcId: { "Fn::ImportValue": !Sub "${ParentVPCStack}-VPC" }

    DMSReplicationInstance: 
      Type: AWS::DMS::ReplicationInstance 
      Properties:  
        AllocatedStorage: !Ref DmsInstanceStorage 
        AutoMinorVersionUpgrade: true 
        AllowMajorVersionUpgrade: true
        EngineVersion: 3.4.4 
        MultiAZ: false 
        PubliclyAccessible: false 
        ReplicationInstanceClass: !Ref ReplicationInstanceClass  
        ReplicationInstanceIdentifier: !Sub ${ReplicationInstanceName}-${Environment}  
        ReplicationSubnetGroupIdentifier: !Ref DMSReplicationSubnetGroup
        VpcSecurityGroupIds: 
          - !Ref DMSSecurityGroup
    
    DmsIngressRuleAttachedToDocDB:
      Type: "AWS::EC2::SecurityGroupIngress"
      Properties:
        GroupId: !Ref ClusterSecurityGroupId
        IpProtocol: tcp
        FromPort: !Ref ClusterPort
        ToPort: !Ref ClusterPort
        SourceSecurityGroupId: !Ref DMSSecurityGroup
        Description: DMS Instance SG services
    
    DMSReplicationTaskAuroraToS3:  
      Type: AWS::DMS::ReplicationTask  
      Properties:  
        MigrationType: full-load-and-cdc 
        ReplicationInstanceArn: !Ref DMSReplicationInstance  
        ReplicationTaskIdentifier: !Sub aurora-to-s3-${Environment} 
        ReplicationTaskSettings: '{  
          "TargetMetadata": {  
            "SupportLobs": false  
          },  
          "Logging" : {  
            "EnableLogging" : true  
          },  
          "FullLoadSettings":{  
            "TargetTablePrepMode": "DROP_AND_CREATE", 
            "StopTaskCachedChangesApplied": false, 
            "StopTaskCachedChangesNotApplied": false, 
            "MaxFullLoadSubTasks": 10,  
            "CommitRate": 5000  
          }, 
          "StreamBufferSettings": { 
              "StreamBufferCount": 10, 
              "StreamBufferSizeInMB": 50, 
              "CtrlStreamBufferSizeInMB": 8 
          }, 
          "ErrorBehavior": { 
              "RecoverableErrorCount": 0, 
              "FullLoadIgnoreConflicts": true 
          } 
        }'  
        SourceEndpointArn: !Ref DMSSourceEndpointAurora
        TargetEndpointArn: !Ref DMSTargetEndpointS3FullAndCdc 
        TableMappings: '{  
          "rules": [  
              { 
                  "rule-type": "selection", 
                  "rule-id": "1", 
                  "rule-name": "1", 
                  "object-locator": { 
                      "schema-name": "public", 
                      "table-name": "%" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              },
              { 
                  "rule-type": "selection", 
                  "rule-id": "2", 
                  "rule-name": "2", 
                  "object-locator": { 
                      "schema-name": "ppm", 
                      "table-name": "%" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              }
          ] 
        }' 
    DMSCloudWatchLogGroup:
      Type: AWS::Logs::LogGroup
      Properties: 
        RetentionInDays: 30 
    DMSCloudWatchLogStream:
      Type: AWS::Logs::LogStream
      Properties: 
        LogGroupName: !Ref DMSCloudWatchLogGroup
    DMSReplicationTaskDocDBToS3:  
      Type: AWS::DMS::ReplicationTask  
      Properties:  
        MigrationType: full-load-and-cdc 
        ReplicationInstanceArn: !Ref DMSReplicationInstance  
        ReplicationTaskIdentifier: !Sub omr-to-s3-${Environment} 
        ReplicationTaskSettings: '{  
          "TargetMetadata": {  
            "SupportLobs": true  
          },  
          "FullLoadSettings":{  
            "TargetTablePrepMode": "DROP_AND_CREATE", 
            "StopTaskCachedChangesApplied": false, 
            "StopTaskCachedChangesNotApplied": false, 
            "MaxFullLoadSubTasks": 1,  
            "CommitRate": 5000  
          }, 
          "StreamBufferSettings": { 
              "StreamBufferCount": 10, 
              "StreamBufferSizeInMB": 50, 
              "CtrlStreamBufferSizeInMB": 8 
          }, 
          "ErrorBehavior": { 
              "RecoverableErrorCount": 0, 
              "FullLoadIgnoreConflicts": true 
          },
          "Logging": {
              "EnableLogging": true
          }
        }'  
        SourceEndpointArn: !Ref DMSSourceEndpointDocumentDB
        TargetEndpointArn: !Ref DMSTargetEndpointS3FullAndCdc 
        TableMappings: '{  
          "rules": [  
              { 
                  "rule-type": "selection", 
                  "rule-id": "1", 
                  "rule-name": "1", 
                  "object-locator": { 
                      "schema-name": "%", 
                      "table-name": "merchant_applications" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              },
              { 
                  "rule-type": "selection", 
                  "rule-id": "2", 
                  "rule-name": "2", 
                  "object-locator": { 
                      "schema-name": "%", 
                      "table-name": "terminal_masters" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              }
          ] 
        }' 
    DMSReplicationTaskDocDB40ToS3:  
      Type: AWS::DMS::ReplicationTask  
      Properties:  
        MigrationType: cdc 
        ReplicationInstanceArn: !Ref DMSReplicationInstance  
        ReplicationTaskIdentifier: !Sub docdb40-to-s3-${Environment} 
        ReplicationTaskSettings: '{  
          "TargetMetadata": {  
            "SupportLobs": true  
          },  
          "FullLoadSettings":{  
            "TargetTablePrepMode": "DO_NOTHING", 
            "StopTaskCachedChangesApplied": false, 
            "StopTaskCachedChangesNotApplied": false, 
            "MaxFullLoadSubTasks": 1,  
            "CommitRate": 5000  
          }, 
          "StreamBufferSettings": { 
              "StreamBufferCount": 10, 
              "StreamBufferSizeInMB": 50, 
              "CtrlStreamBufferSizeInMB": 8 
          }, 
          "ErrorBehavior": { 
              "RecoverableErrorCount": 0, 
              "FullLoadIgnoreConflicts": true 
          },
          "Logging": {
              "EnableLogging": true
          }
        }'  
        SourceEndpointArn: !Ref DMSSourceEndpointDocumentDB40
        TargetEndpointArn: !Ref DMSTargetEndpointS3FullAndCdc 
        TableMappings: '{  
          "rules": [  
              { 
                  "rule-type": "selection", 
                  "rule-id": "1", 
                  "rule-name": "1", 
                  "object-locator": { 
                      "schema-name": "%", 
                      "table-name": "merchant_applications" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              },
              { 
                  "rule-type": "selection", 
                  "rule-id": "2", 
                  "rule-name": "2", 
                  "object-locator": { 
                      "schema-name": "%", 
                      "table-name": "terminal_masters" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              }
          ] 
        }' 
    DMSReplicationTaskDocDBUsersToS3:  
      Type: AWS::DMS::ReplicationTask  
      Properties:  
        MigrationType: full-load-and-cdc 
        ReplicationInstanceArn: !Ref DMSReplicationInstance  
        ReplicationTaskIdentifier: !Sub omr-users-to-s3-${Environment} 
        ReplicationTaskSettings: '{  
          "TargetMetadata": {  
            "SupportLobs": true  
          },  
          "FullLoadSettings":{  
            "TargetTablePrepMode": "DROP_AND_CREATE", 
            "StopTaskCachedChangesApplied": false, 
            "StopTaskCachedChangesNotApplied": false, 
            "MaxFullLoadSubTasks": 1,  
            "CommitRate": 5000  
          }, 
          "StreamBufferSettings": { 
              "StreamBufferCount": 10, 
              "StreamBufferSizeInMB": 50, 
              "CtrlStreamBufferSizeInMB": 8 
          }, 
          "ErrorBehavior": { 
              "RecoverableErrorCount": 0, 
              "FullLoadIgnoreConflicts": true 
          },
          "Logging": {
              "EnableLogging": true
          }
        }'  
        SourceEndpointArn: !Ref DMSSourceEndpointDocumentDB
        TargetEndpointArn: !Ref DMSTargetEndpointS3FullAndCdc 
        TableMappings: '{  
          "rules": [  
              { 
                  "rule-type": "selection", 
                  "rule-id": "1", 
                  "rule-name": "1", 
                  "object-locator": { 
                      "schema-name": "%", 
                      "table-name": "user_masters" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              },
              { 
                  "rule-type": "selection", 
                  "rule-id": "2", 
                  "rule-name": "2", 
                  "object-locator": { 
                      "schema-name": "%", 
                      "table-name": "role_masters" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              }
          ] 
        }'  
    DMSReplicationTaskDocDB40UsersToS3:  
      Type: AWS::DMS::ReplicationTask  
      Properties:  
        MigrationType: full-load-and-cdc 
        ReplicationInstanceArn: !Ref DMSReplicationInstance  
        ReplicationTaskIdentifier: !Sub docdb40-users-to-s3-${Environment} 
        ReplicationTaskSettings: '{  
          "TargetMetadata": {  
            "SupportLobs": true  
          },  
          "FullLoadSettings":{  
            "TargetTablePrepMode": "DROP_AND_CREATE", 
            "StopTaskCachedChangesApplied": false, 
            "StopTaskCachedChangesNotApplied": false, 
            "MaxFullLoadSubTasks": 1,  
            "CommitRate": 5000  
          }, 
          "StreamBufferSettings": { 
              "StreamBufferCount": 10, 
              "StreamBufferSizeInMB": 50, 
              "CtrlStreamBufferSizeInMB": 8 
          }, 
          "ErrorBehavior": { 
              "RecoverableErrorCount": 0, 
              "FullLoadIgnoreConflicts": true 
          },
          "Logging": {
              "EnableLogging": true
          }
        }'  
        SourceEndpointArn: !Ref DMSSourceEndpointDocumentDB40
        TargetEndpointArn: !Ref DMSTargetEndpointS3FullAndCdc 
        TableMappings: '{  
          "rules": [  
              { 
                  "rule-type": "selection", 
                  "rule-id": "1", 
                  "rule-name": "1", 
                  "object-locator": { 
                      "schema-name": "%", 
                      "table-name": "user_masters" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              },
              { 
                  "rule-type": "selection", 
                  "rule-id": "2", 
                  "rule-name": "2", 
                  "object-locator": { 
                      "schema-name": "%", 
                      "table-name": "role_masters" 
                  }, 
                  "rule-action": "include", 
                  "filters": [] 
              }
          ] 
        }'                
# Outputs:
#   DMSTargetEndpointS3FullAndCdc  
#   ClusterSecurityGroupId:
#     Description: DMS S3 export dump bucket
#     Value: !GetAtt DMSTargetEndpointS3FullAndCdc.GroupId
#     Export:
#       Name: "DocumentDB-App-ClusterSecurityGroupId"  